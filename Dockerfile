FROM centos:6.9
MAINTAINER Manoj Kulkarni <manoj.kulkarni@valuelabs.com>

# install mysql
RUN yum install -y mysql mysql-server
RUN echo "NETWORKING=yes" > /etc/sysconfig/network
# start mysqld to create initial tables
RUN service mysqld start

# install php
RUN yum install -y php php-mysql php-devel php-gd php-pecl-memcache php-pspell php-snmp php-xmlrpc php-xml

ADD src/ /var/www/html/
EXPOSE 80

COPY phpinfo.php /var/www/app/public/
RUN ln -s /var/www/app/public/phpinfo.php /var/www/app/public/index.php